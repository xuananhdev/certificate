const express = require("express");
const cors = require("cors");
const renderCertificate = require("./renderCertificate");

const PORT = 9000;
const app = express();

// app.use(
//   cors({
//     credentials: true,
//     origin: ["http://localhost:3000", "http://localhost:4000", "http://localhost:5000", "https://thvv.sunshine.software"],
//   })
// );
app.use(cors());

//MIDDLE_WARE
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post("/api/certificate", async (req, res, next) => {
  const { json, background } = req.body;
  if (!json || !background) {
    return res.status(400).json({
      message: "Please check info body json or background is null!!",
    });
  }

  try {
    const imgBase64 = await renderCertificate(json, background);
    return res.status(200).json({
      status: 200,
      imgBase64,
    });
  } catch (error) {
    return res.status(400).json({
      status: 500,
      message: "Server error!!",
    });
  }
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error("Not Found");
  console.log(err);
  err.status = 404;
  res.send("Route not found");
  next(err);
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
