// const { loadImage } = require("canvas");
var fs = require("fs");
const request = require("request");

// relative path here
// but you will need just require('konva-node');
const Konva = require("konva/cmj").default;
var Request = require("pixl-request");
const { registerFont, createCanvas } = require("canvas");
const { uuid } = require("uuidv4");

registerFont("../statics/fonts/iCielBrandonText-Bold.ttf", {
  family: "Brandon Text",
});

var download = function (uri, filename, callback) {
  request.head(uri, function (err, res, body) {
    // console.log("content-type:", res.headers["content-type"]);
    // console.log("content-length:", res.headers["content-length"]);
    request(uri).pipe(fs.createWriteStream(filename)).on("close", callback);
  });
};

async function renderCertificate(jsonFile, imgUrl) {
  return new Promise((resolve, reject) => {
    try {
      const randomKey = uuid();
      const nameFile = `${Date.now()}_${randomKey}`;
      download(imgUrl, `tmp/${nameFile}.png`, async function () {
        // console.log("done");
        var stage = new Konva.Stage({
          width: jsonFile.attrs.width,
          height: jsonFile.attrs.height,
        });

        var layer = new Konva.Layer();

        stage.add(layer);

        const promiseImage = async () => {
          return new Promise((resolve, reject) => {
            try {
              Konva.Image.fromURL(`tmp/${nameFile}.png`, function (image) {
                // const imgConfig = jsonFile.children[0].children.find((child) => child.className === "Image");
                // console.log("imgConfig", imgConfig);
                image.setAttrs({
                  // ...imgConfig.attrs,
                  x: 0,
                  y: 0,
                  width: jsonFile.attrs.width,
                  height: jsonFile.attrs.height,
                  scaleStageScale: 0,
                  scaleStageX: 0,
                  scaleStageY: 0,
                });
                layer.add(Konva.Node.create(image, "container"));
              });
              return resolve(true);
            } catch (error) {
              console.log("add image background error:", error);
            }
          });
        };

        await promiseImage();

        for (let i = 0; i < jsonFile.children[0].children.length; i++) {
          if (jsonFile.children[0].children[i].className === "Text") {
            let text = new Konva.Text(jsonFile.children[0].children[i].attrs);
            layer.add(text);
          }
        }

        layer.draw();

        stage.toDataURL({
          callback: function (data) {
            var base64Data = data.replace(/^data:image\/png;base64,/, "");

            // fs.writeFile(`${nameFile}.png`, base64Data, "base64", function (err) {
            //   err && console.log(err);
            //   // console.log("See out.png");
            // });

            fs.unlink(`tmp/${nameFile}.png`, (err) => {
              if (err) throw err; //handle your error the way you want to;
              // console.log("path/file.txt was deleted"); //or else the file will be deleted
            });

            return resolve(base64Data);
          },
        });
      });
    } catch (error) {
      return reject(error);
    }
  });
}

module.exports = renderCertificate;
