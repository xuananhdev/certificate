const Konva = require("konva/cmj").default;
const canvas = require("canvas");
const fs = require("fs");

let json = {
  attrs: {
    width: 600,
    height: 600,
  },
  className: "Stage",
  children: [
    {
      attrs: {},
      className: "Images",
      children: [
        {
          attrs: {
            src: "https://s3-sgn09.fptcloud.com/social/STNH%C4%90/V2/upload/32156/files/20220818130874_TGTiLeCaoNhat.png",
            width: 600,
            height: 600,
            alt: "",
          },
          className: "Image",
        },
      ],
    },
    {
      attrs: {},
      className: "Layer",
      children: [
        {
          attrs: {
            scaleStageScale: 1,
            scaleStageX: 0,
            scaleStageY: 0,
            align: "center",
            x: 276.1224885485083,
            y: 325.26304959187877,
            width: 100,
            nodeId: "txtDate",
            text: "[date]",
            color: "#3E362F",
            fontSize: 14,
            fill: "#3E362F",
            fontFamily: "Brandon Text",
            fontWeight: 500,
            letterSpacing: 0.644714,
            draggable: true,
          },
          className: "Text",
        },
        {
          attrs: {
            scaleStageScale: 1,
            scaleStageX: 0,
            scaleStageY: 0,
            align: "center",
            x: 125.05295557548291,
            y: 160.09808628849714,
            width: 400,
            nodeId: "txtUserName",
            text: "[user_name]",
            color: "#FD8A28",
            fontSize: 34,
            fill: "#FD8A28",
            fontFamily: "Brandon Text",
            fontWeight: 450,
            letterSpacing: 0.644714,
            draggable: true,
          },
          className: "Text",
        },
        {
          attrs: {
            scaleStageScale: 1,
            scaleStageX: 0,
            scaleStageY: 0,
            align: "center",
            x: 124.05249509221787,
            y: 258.16763838397696,
            width: 400,
            nodeId: "txtValue",
            text: "[value]",
            color: "#FD8A28",
            fontSize: 44,
            fill: "#FD8A28",
            fontFamily: "Brandon Text",
            fontWeight: 450,
            letterSpacing: 0.644714,
            draggable: true,
          },
          className: "Text",
        },
      ],
    },
    {
      attrs: {},
      className: "Images",
      children: [
        {
          attrs: {
            src: "https://s3-sgn09.fptcloud.com/social/STNH%C4%90/V2/upload/32156/files/20220818130874_TGTiLeCaoNhat.png",
            width: 600,
            height: 600,
          },
          className: "Image",
        },
      ],
    },
  ],
};

let types = ["pdf", "svg", "png"];
function newExport(json) {
  let { width, height } = json.attrs;
  let background = json.children[0].children[0];
  let logo = json.children[1];
  //   let watermark = json.children[2].children[0];
  logo.children.unshift(background);
  //   logo.children.push(watermark);
  for (let type of types) {
    let canvases = [];
    Konva.Util.createCanvasElement = () => {
      const node = new canvas.Canvas(width, height, type);
      node.style = {};
      canvases.push(node);
      return node;
    };

    const stage = new Konva.Stage({ width, height });

    stage.add(Konva.Node.create(logo, "container"));
    let layer = canvases.pop();
    layer = canvases.pop();
    fs.writeFile("out." + type, layer.toBuffer(), (r) => {});
  }
}
newExport(json);
